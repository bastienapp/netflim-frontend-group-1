import CommentForm from "../../components/CommentForm";
import NextBackButtons from "../../components/NextBackButtons";
import Comments from "../../components/comment";
import "./MovieDetailsPage.css";
import MovieDetailFilm from "../../components/MovieDetailFilm";
import Category from "../../components/Category";
import AddButton from "../../components/AddButton";
import Rating from "../../components/Rating";

function MovieDetailsPage() {
  return (
    <>
      <div className="back-page--button">
        <NextBackButtons onClick={""} label="Retour"></NextBackButtons>
      </div>
      <MovieDetailFilm
        title="Mission Impossible : Dead reckoning"
        director="Christopher McQuarrie"
        releaseDate="(2023)"
        duration="1h63min"
        description="Ethan Hunt, au service de Mission Impossible depuis des décennies, fait face à ennemi d'un nouveau genre: une intelligence artificielle nommée « l'Entité », intangible, omniprésente, capable d'influencer comme jamais notre monde d'aujourd'hui. Entré en possession d'un indice qui pourrait permettre à quiconque parvenant à suivre la piste de contrôler le destin de cette I.A., Ethan Hunt ne suit plus les ordres, mais s'assigne lui-même une mission: mettre un terme à la menace de l'Entité par tous les moyens. Mais alors qu'il est traqué par le monde entier, Ethan va découvrir que cette Entité connaît tout de lui, s'est alliée à des ennemis de son passé d'avant Mission Impossible, et que pour la vaincre et sauver l'avenir, il devra sacrifier ce qu'il a de plus cher au monde."
      />
      <Category type="Fantastique" />
      <Category type="Thriller" />
      <Rating rating="4.5" />
      <AddButton />
      <Comments
        title="Commentaire"
        avatar="./bernard.jpg"
        name="Billy John"
        comment="Ce film c'est trop de la bombe !"
      />
      <div className="buttons--container">
        <CommentForm />
        <div className="previous--movie-button">
          <NextBackButtons
            onClick={""}
            label="Film précédent"
          ></NextBackButtons>
        </div>
        <div className="next--movie-button">
          <NextBackButtons onClick={""} label="film suivant"></NextBackButtons>
        </div>
      </div>
    </>
  );
}

export default MovieDetailsPage;

import { useOutletContext } from "react-router-dom";
import CategoryCard from "../../components/CategoryCard";
import MovieList from "../../components/MovieList";
import { useState } from "react";


function HomePage() {

  // Appel du tableau liste de films
  // const movieList = PopularMovieList.results
  const { movieList, categories } = useOutletContext();

  // Lifting state pour pouvoir l'utiliser dans les enfants de homepage
  const [isSelected, setIsSelected] = useState(false)


  return (
    <>
      <div className="home-page-container">

        {/*Liste de films, on fait passer les props qu'on a déclaré plus haut dans les parents*/}
        <h1>Team Rocket, rendez-vous tous, ou ce sera la guerre!</h1>
        <CategoryCard
          categories={categories}
          isSelected={isSelected}
          setIsSelected={setIsSelected}
          title="Catégories"
        />

        {/* Passage des props du tableau dans une variable movieList */}
        <MovieList
          movieList={movieList}
          categories={categories}
          isSelected={isSelected}
          setIsSelected={setIsSelected}
        />

      </div>

    </>
  );
}

export default HomePage;

import ListFilm from "../../components/ListFilm";
import "./ProfilePage.css";
import Recommend from "../../components/Recommend";
import '../../components/Recommend.css'
import { useOutletContext } from "react-router-dom";
import AvatarInfo from "../../components/AvatarInfo";

function ProfilePage() {

  const {movieList, categories} = useOutletContext()


  return (
    <>
      {/* Page de profil */}

      <div className="profile-page-container">
        <div className="personnal-info-container">
          <AvatarInfo />

          <Recommend movieList={movieList}  />
        </div>

        <h1 className="list-title">Films déjà vus</h1>
        <ListFilm movieList={movieList} />
        <h1 className="list-title">J'aimerai voir</h1>
        <ListFilm movieList={movieList} />

      </div>
    </>
  );
}

export default ProfilePage;

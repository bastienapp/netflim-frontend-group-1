import MovieCard from "./MovieCard";

function MovieList(props) {

  const { movieList } = props;
  const { isSelected } = props

  const filteredMovies = isSelected ? movieList.filter(movie => {
    return movie.genre_ids.includes(isSelected)
  } ) : movieList


  return (
    <>
      <section className="movie-list">
        {filteredMovies.map((eachmovie) => (
          <MovieCard
            key={eachmovie.id}
            id={eachmovie.id}
            title={eachmovie.title}
            rating={eachmovie.vote_average}
            poster={eachmovie.poster_path}
            movieCategoryids={eachmovie.genre_ids}
          />
        ))}
      </section>
    </>
  );
}

export default MovieList;
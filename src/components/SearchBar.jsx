import React, {useState } from "react";

function SearchBar() {
    const [searchTerm, setSearchTerm] = useState("");

    const handleInputChange = (event) => {
        setSearchTerm(event.target.value);
    };

};


export default SearchBar;
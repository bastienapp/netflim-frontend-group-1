import "./Category.css";
import { useState } from "react";

function Category(props) {

    const { category, categoryID } = props;
    const { isSelected, setIsSelected } = props

    // initialisation du useState pour le css
    // const [isSelected, setIsSelected] = useState(false);
    
    //état selected, quand le bouton est selectionné, s'affiche en bleu
    function select() {
        isSelected ? setIsSelected(false) : setIsSelected( categoryID );
    }
    
    return (
        <>
            <button onClick={() => select()}
                className={`category-button ${isSelected ? 'selected' : ''}`}
                id={categoryID}>
                {category}
            </button>
        </>
    )
}

export default Category;

{/* {categoriess.map((eachcategories) => (
            <button className={`category-button ${isSelected ? 'selected' : ''}`} onClick={selected}>
                
            </button>
        ))} */}
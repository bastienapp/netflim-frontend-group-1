import './ProfilAvatar.css'

function ProfilAvatar(props) {

    const { avatar, pseudo } = props;



    return (
            <div className='avatar--container'>
              <img className="avatar" src={avatar} alt="Avatar" />
              <h2 className="pseudo--title">{pseudo}</h2>
            </div>
          );
        }


export default ProfilAvatar 
import { useState } from "react";
import "./CommentForm.css";

function CommentForm({ comment }) {
  const [addComment, setAddComment] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    comment(addComment);
    setAddComment("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        <h4 className="title--addcomment">Ajouter un commentaire</h4>
        <input
          className="placeholder--addcomment"
          type="text"
          placeholder="Ecrivez votre commentaire ici "
          value={addComment}
          onChange={(event) => setAddComment(event.target.value)}
          required
        />
      </label>
      <button className="button--addcomment" type="submit">
        Envoyer
      </button>
    </form>
  );
}

export default CommentForm;

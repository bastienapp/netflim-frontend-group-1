import './InformationsProfil.css'

function InformationsProfil(props) {
  const { dateNaissance, dateCreation } = props;

  return (
    <div className="profil--informations">
      <p className="birth--date-information">Date de naissance :<br /> {dateNaissance}</p>
      <p className='creation--compte-information'>Création de compte :<br /> {dateCreation}</p>
    </div>
  );
}

export default InformationsProfil;

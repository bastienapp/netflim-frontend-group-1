import ButtonsProfil from "./ButtonsProfil"
import InformationsProfil from "./InformationsProfil"
import ProfilAvatar from "./ProfilAvatar"
import './AvatarInfo.css'

function AvatarInfo() {


    const handleEditProfile = () => { };
    const handleLegoutProfile = () => { };
    const handleModifyProfile = () => { };
    const handleDeleteProfile = () => { };

    return (
        <div className="profile-container">
            <div className="profile-info">
                <ProfilAvatar avatar="./bernard.jpg" pseudo="Billy" />
                <div className="profil--informations">
                    <InformationsProfil
                        dateNaissance="18/02/1985"
                        dateCreation="02/12/2021"
                    />
                </div>
            </div>
            <div className="profile-buttons">
                <ButtonsProfil
                    onClick={handleEditProfile}
                    label="Modifier"
                />
                <ButtonsProfil
                    onClick={handleLegoutProfile}
                    label="Déconnexion"
                />
                <ButtonsProfil
                    onClick={handleModifyProfile}
                    label="Modification"
                />
                <ButtonsProfil
                    onClick={handleDeleteProfile}
                    label="Supprimer"
                />
            </div>
        </div>
    )
}

export default AvatarInfo
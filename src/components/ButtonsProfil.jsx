import './ButtonsProfil.css'

function ButtonsProfil({ onClick, label }) {

        return (
          <button className="buttons--profil" onClick={onClick}>
            {label}
          </button>
        );
      }
      


export default ButtonsProfil
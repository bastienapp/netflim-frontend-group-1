
// import PopularMovieList from "../data/popularMovieList"


function Recommend (props) {

    const { movieList } = props

    return (
        <>
            <div className="bloc-recommande">
                <div className="bloc-text">
                    <h2>Je recommande</h2>
                    <h3>{movieList[3].title}</h3>
                    <p>Lorsque Carol Danvers, alias Captain Marvel, se retrouve dans un étrange vortex lié à une révolution kree, ses pouvoirs  se mêlent à ceux de Kamala Khan, alias Miss Marvel, et à ceux de sa nièce, le capitaine Monica Rambeau, désormais astronaute du S.A.B.E.R. Ce trio improbable va devoir travailler ensemble pour sauver l'univers.</p>
                </div>
                <img className="image-recommande" src="https://image.tmdb.org/t/p/w1280/mqAQO6j5gkq6iwCXNbXpzf0RXBU.jpg" alt=""/>
            </div>
        </>
    )
}

export default Recommend
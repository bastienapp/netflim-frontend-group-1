import addButton from '../assets/Add-icon.png'
import addedButton from '../assets/coche.png'
import { useState } from 'react'
import './AddButton.css'

function AddButton(props) {
    const {id} = props;
    
    const currentFavoriteValue = localStorage.getItem('statusFavorite-'+ id) === "true" ? true : false;

    // initialisation du State :
    const [isFavorite, setIsFavorite] = useState(currentFavoriteValue);

    
    function toggleStatus () {
        const newStatusF = !isFavorite
        //change l'état
        setIsFavorite(newStatusF);
        //enregiste dans localStorage avec .setItem('key', value)
        localStorage.setItem('statusFavorite-'+ id, newStatusF)
    }

    return (
        <>
    {/* Création d'un bouton avec une image */}
            <input className='button-picture'id="add-button" type="image" alt="Ajouter à la liste" onClick={toggleStatus} src={isFavorite ? addedButton : addButton}/>
        </>
    )
}

export default AddButton

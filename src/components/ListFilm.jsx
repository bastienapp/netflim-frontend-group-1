import PopularMovieList from "../data/popularMovieList.js"
import MoviePoster from "./MoviePoster.jsx"
import './ListFilm.css'

function ListFilm(props) {
    const movieList = PopularMovieList.results
    
    return (
        <>

            <div className="scroll-container">
                <div className="scroll-movie-poster">
                    {movieList.map((eachmovie) => (
                        <MoviePoster className="movie-poster"
                            key={eachmovie.id}
                            id={eachmovie.id}
                            poster={eachmovie.poster_path}
                            title={eachmovie.title}
                        />
                    ))}
                </div>
            </div>


        </>
    )
}

export default ListFilm

import './NextBackbuttons.css'

function NextBackButtons({ onClick, label, icon }) {
  return (
    <button className="next-back--buttons" onClick={onClick}>
      {label} {icon}
    </button>
  );
}

export default NextBackButtons;

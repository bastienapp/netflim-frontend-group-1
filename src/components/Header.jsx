import "./Header.css";
import Loupe from "../assets/loupe.png";
import shuttle from "../assets/shuttle.png";
import iconUser from "../assets/iconUser.png";


function Header(props) {
  return (
    <header className="header-contenu" >
      <div className="logo" >
        <img src={shuttle} alt="Logo" />
        <h1 className="site-title" >ROCKET</h1>
      </div>
      <div className="search-bar-and-icon" >
        <div className="search-barre" >
          <input type="image" alt="loupe" src={Loupe} />
          <input type="search" id="site-search" placeholder="rechercher" />
        </div>
        <div>
          <img className="icon" src={iconUser} alt="search-icon" />
        </div>
      </div>
    </header>
  );
};

export default Header;
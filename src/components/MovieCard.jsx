import AddButton from './AddButton';
import './MovieCard.css'
import Rating from './Rating';
import { Link } from 'react-router-dom';


function MovieCard(props) {

  // Destructuration des props
  const { title, poster, rating, id } = props

  

  return (
    <>
      <button className='movie-card'>
        <Link to='/film-detail'><img className='movie-img' src={`https://www.themoviedb.org/t/p/w1280${poster}`} alt={title} /></Link>
        <h3 className='movie-title'>{title}</h3>
        {/* Appel du composant Rating en lui faisant passer la propriété à afficher "rating" */}
        <Rating
          rating={rating}
        />
        <AddButton id={id} />
      </button>
    </>
  );
}

export default MovieCard;

function Rating(props) {

    const { rating } = props

    return (
        <>
            <div className="rating-bubble">
                <p>{rating}</p>
            </div>
        </>
    )
}

export default Rating
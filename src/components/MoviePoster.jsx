import './MoviePoster.css';

function MoviePoster(props) {

    const { title, poster } = props
    return (
        <>
            <img className='movie-poster' src={`https://www.themoviedb.org/t/p/w1280${poster}`} alt={title} />
        </>
    )
}
export default MoviePoster
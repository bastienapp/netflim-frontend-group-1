import mission from '../assets/mission.jpg';
import addButton from '../assets/Add-icon.png'
import './MovieDetailFilm.css'



 function MovieDetailFilm(props) {
  const { title, duration, description, releaseDate, director, rating } = props;

  return (
    <>
    <div className='movie-details'>
    <div className='movie-info'>
    <img className='affiche'src={mission} alt="poster"/>
    <div className="rating-bubble">
    <p>{rating}</p>
    </div>
    </div>
    <div className='informations'>
    <h1 className='title'>{title}</h1>
    <div className='details'>
    <p>{director}</p>
    <p>{releaseDate}</p>
    <p>{duration}</p>
    <input id="add-button" type="image" alt="Ajouter à la liste" src={addButton}/>
    </div>
    <p><strong>Synopsis</strong></p>
    <p className='texte'>{description}</p>
    </div>
    </div>
    </>
  );
  }

export default MovieDetailFilm;

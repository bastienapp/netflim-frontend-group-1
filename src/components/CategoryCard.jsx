import "./CategoryCard.css"
import Category from "./Category";
import { useState } from "react";

function CategoryCard(props) {
    const { title, categories, isSelected, setIsSelected, } = props;
    // const [selectedCategories, setSelectedCategories] = useState({})

    return (
        <div className="category-block">
            <h1 className="category-title">{title}</h1>
            <div className="category-elements">
                {categories.map((eachcategory) => (
                    <Category
                        key={eachcategory.id}
                        categoryID={eachcategory.id}
                        category={eachcategory.name}
                        // Pour l'instant je compare la valeur de isSelected avec le nom de la catégorie, ça me permet d'avoir un bouton par catégorie et ils ne sont pas tous cliqués en même temps. Par la suite je voudrais qu'il soit possible de selectionner plusieurs categories en même temps, il faudra peut-être ajouter le state à une serie d'autre state ?
                        isSelected={isSelected === eachcategory.id}
                        setIsSelected={setIsSelected}
                    />
                ))}
            </div>
        </div>
    )
}

export default CategoryCard;


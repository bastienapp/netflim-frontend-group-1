import "./Comment.css";

function Comments(props) {
  const { title, avatar, name, comment } = props;

  return (
    <div className="comment--block">
      <h3 className="title--comment">{title}</h3>
      <div className="avatar--name-container">
        <img className="avatar--comments" src={avatar} alt="Avatar" />
        <h4 className="name">{name}</h4>
      </div>
      <p className="comment">{comment}</p>
    </div>
  );
}

export default Comments;

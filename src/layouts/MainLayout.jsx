
import Header from '../components/Header';
import { Outlet } from "react-router-dom";
import PopularMovieList from "../data/popularMovieList"
import Categories from "../data/category"

//Bienvenue dans le layout, ici tout se répète sauf Outlet! #Métamorphe
function MainLayout() {

  const movieList = PopularMovieList.results
  const categories = Categories

  return (
    <>
      <Header />
      <main>
        {/* ici je veux afficher le contenu des différentes pages */}
        <Outlet context={ {movieList, categories} }/>
      </main>
      <footer>
        ©2023 Team Rocket, plus rapide que la lumière
      </footer>
    </>
  )
}

export default MainLayout;

import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { createBrowserRouter, RouterProvider, } from "react-router-dom";
import HomePage from "./pages/Home/HomePage.jsx";
import MainLayout from "./layouts/MainLayout";
import ProfilePage from "./pages/Profile/ProfilePage";
import MovieDetailsPage from "./pages/MovieDetails/MovieDetailsPage.jsx";
const router = createBrowserRouter(
  
  [
    {
      path: "/", //page d'accueil
      element: <MainLayout/>,
      children: [
        //lien vers HomePage 'liste de films' par défaut
         {
          path: "/",
          element: <HomePage/>,
         },
        //lien vers la page profil
         {
          path: "/profile",
          element: <ProfilePage/>,
         },
         //lien vers la page Détails de films
         //boucle pour chaque page de film différent
         {
          /*path => objet + id*/
          path: "/film-detail",
          element: <MovieDetailsPage/>
         }
      ]

    }
  ]
)


ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router}/>
  </React.StrictMode>
);
